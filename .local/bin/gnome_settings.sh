#!/bin/sh

# XKB options
gsettings set org.gnome.desktop.input-sources xkb-options "['ctrl:swapcaps']"

# Keyboard
gsettings set org.gnome.desktop.peripherals.keyboard delay 200
gsettings set org.gnome.desktop.peripherals.keyboard numlock-state true
gsettings set org.gnome.desktop.peripherals.keyboard repeat-interval 40

# Touchpad
gsettings set org.gnome.desktop.peripherals.touchpad natural-scroll false
